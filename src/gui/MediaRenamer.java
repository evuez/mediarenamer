package gui;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

// TODO #1 ABORTED User is waiting for something when he click on a button: give him that thing: CALL TO ACTION
// TODO #2 Create Internet Connection Class to manage/test connections to API,...
// TODO #3 Portable DB http://hsqldb.org/ , SQLite
// TODO rules.xml: add valid extensions to rename
// Music API: LastFM, http://www.lastfm.fr/api/show/track.getInfo

/**
 * Main class managing GUI and elements used in it.
 * 
 * @author evuez
 *
 */
public class MediaRenamer extends JFrame {

	private static final long serialVersionUID = 1L;
	
	public static MediaRenamer MediaRenamerGUI;
	
	public static uimanagers.Pilot PilotUIManager;
	private static uimanagers.Renamer RenamerUIManager = new uimanagers.Renamer();
	private static uimanagers.Options OptionsUIManager = new uimanagers.Options();
	private static uimanagers.Log LogUIManager = new uimanagers.Log();
	
	private String LogPath = main.Settings.LogPath;
	
	private static JTabbedPane MainTabbedPane;
	private JComponent TabRenamer;
	private JComponent TabOptions;
	private JComponent TabLog;
	
	public static JComboBox RenamerMediaList;
	public static JTextField RenamerDirectoryField;
	public static JButton RenamerChooseDirectory;
	public static JButton RenamerScanDirectory;
	public static JButton RenamerCheckErrors;
	public static JButton RenamerRenameSelected;
	public static DefaultTableModel DataTableModel;
	public static JScrollPane RenamerTableContainer;
	public static JProgressBar RenamerScanProcess;
	
	public static JComboBox OptionsMediaList;
	public static JComboBox OptionsLanguagesList;
	public static JComboBox OptionsDatasourcesList;
	public static JTextField OptionsOutputFormatField;
	public static JComboBox OptionsDataList;
	public static JButton OptionsAddData;
	public static JButton OptionsRemoveData;
	public static JButton OptionsSaveAsDefault;
	public static JButton OptionsLoadDefault;
	
	private static JScrollPane LogTableContainer;
	
	
	public static void main(String[] args) {
		
		// Change Default Look and Feel to System Look and Feel
		/*
		try {UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());} 
		catch (UnsupportedLookAndFeelException e) {}
		catch (ClassNotFoundException e) {}
		catch (InstantiationException e) {}
		catch (IllegalAccessException e) {}
		//*/
		
		MediaRenamer MediaRenamerGUI = new MediaRenamer();
		MediaRenamerGUI.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		MediaRenamerGUI.setVisible(true);

	}
	
	public MediaRenamer() {
		
		setTitle("MediaRenamer");
		setSize(750, 570);
		setResizable(false);
		setLocationRelativeTo(null);
		
		Container layout = getContentPane();
		layout.setLayout(new FlowLayout(FlowLayout.LEFT, 0, 0));
		
		
		MainTabbedPane = new JTabbedPane();
		MainTabbedPane.addTab("Renamer", new JPanel());
		MainTabbedPane.addTab("Options", new JPanel());
		MainTabbedPane.addTab("Log", new JPanel());
		MainTabbedPane.setPreferredSize(new Dimension(744, 541));
		
		PilotUIManager = new uimanagers.Pilot(MainTabbedPane, Color.LIGHT_GRAY);
		
		TabRenamer = ((JComponent) MainTabbedPane.getComponentAt(0));
		TabOptions = ((JComponent) MainTabbedPane.getComponentAt(1));
		TabLog = ((JComponent) MainTabbedPane.getComponentAt(2));
		
		TabRenamer.setLayout(new BorderLayout());
		TabOptions.setLayout(new BorderLayout());
		TabLog.setLayout(new BorderLayout());
		
		// TabRenamer
		TabRenamer.add(panelRenamerSettings("Settings"), BorderLayout.NORTH);
		TabRenamer.add(panelRenamerMain("Main"), BorderLayout.CENTER);
		
		// TabOptions
		TabOptions.add(panelOptionsMedia("Media"), BorderLayout.NORTH);
		TabOptions.add(panelOptionsGeneral("General"), BorderLayout.CENTER);
				/*JPanel // DB storage, personal API (maybe later)*/
		
		// TabLog
		TabLog.add(panelLogMain(), BorderLayout.CENTER);
		

		layout.add(MainTabbedPane);
		
		bindListeners();
		
		/**
		 * Will be called in every try...catch statements
		 * example:
		 * 	try{
		 * 		instructions
		 * 		...
		 * 	} catch(Exception e) {
		 * 		LogManager.UpdateLog(PathToMainLog, e.getMessage());
		 * 		LogManager.UpdateLogTable(PathToMainLog);
		 * 	}
		 */
		LogUIManager.updateLogTable(LogPath);

	}
	
	private static JPanel panelRenamerSettings(String Title) {
		
		JPanel Panel = new JPanel();
		Panel.setBorder(BorderFactory.createTitledBorder(Title));
		
		RenamerMediaList = RenamerUIManager.mediaList();
		RenamerDirectoryField = RenamerUIManager.directoryField();
		RenamerChooseDirectory = RenamerUIManager.chooseDirectory();
		
		RenamerMediaList.setPreferredSize(new Dimension(96, 25));
		RenamerDirectoryField.setPreferredSize(new Dimension(418, 25));
		RenamerChooseDirectory.setPreferredSize(new Dimension(200, 25));
		
		Panel.add(RenamerMediaList);
		Panel.add(RenamerDirectoryField);
		Panel.add(RenamerChooseDirectory);
		
		return Panel;
		
	}
	
	private static JPanel panelRenamerMain(String Title) {
		
		JPanel Panel = new JPanel();
		Panel.setBorder(BorderFactory.createTitledBorder(Title));
		
		RenamerScanDirectory = RenamerUIManager.scanDirectory();
		RenamerCheckErrors = RenamerUIManager.checkErrors();
		RenamerRenameSelected = RenamerUIManager.renameSelected();
		DataTableModel = RenamerUIManager.dataTableModel();
		RenamerTableContainer = new JScrollPane(RenamerUIManager.dataTable(DataTableModel));
		RenamerScanProcess = RenamerUIManager.mainProcess();
		
		RenamerScanDirectory.setPreferredSize(new Dimension(238, 25));
		RenamerCheckErrors.setPreferredSize(new Dimension(238, 25));
		RenamerRenameSelected.setPreferredSize(new Dimension(238, 25));
		RenamerTableContainer.setPreferredSize(new Dimension(726, 360));
		RenamerScanProcess.setPreferredSize(new Dimension(725, 25));
		
		Panel.add(RenamerScanDirectory);
		Panel.add(RenamerCheckErrors);
		Panel.add(RenamerRenameSelected);
		Panel.add(RenamerTableContainer);
		Panel.add(RenamerScanProcess);
		
		return Panel;
		
	}
	
	private static JPanel panelOptionsMedia(String Title) {
		
		JPanel Panel = new JPanel();
		Panel.setBorder(BorderFactory.createTitledBorder(Title));
		
		OptionsMediaList = OptionsUIManager.mediaList();
		OptionsMediaList.setPreferredSize(new Dimension(723, 25));
		
		Panel.add(OptionsMediaList);
		
		return Panel;
		
	}
	
	private static JPanel panelOptionsGeneral(String Title) {
		
		JPanel Panel = new JPanel();
		Panel.setBorder(BorderFactory.createTitledBorder(Title));
		
		OptionsLanguagesList = OptionsUIManager.languagesList((String) OptionsMediaList.getSelectedItem());
		OptionsDatasourcesList = OptionsUIManager.datasourcesList((String) OptionsMediaList.getSelectedItem(), (String) OptionsLanguagesList.getSelectedItem());
		OptionsOutputFormatField = OptionsUIManager.outputFormatField();
		OptionsDataList = OptionsUIManager.dataList((String) OptionsMediaList.getSelectedItem(), (String) OptionsLanguagesList.getSelectedItem(), (String) OptionsDatasourcesList.getSelectedItem());
		OptionsAddData = OptionsUIManager.addData();
		OptionsRemoveData = OptionsUIManager.removeData();
		OptionsSaveAsDefault = OptionsUIManager.saveAsDefault();
		OptionsLoadDefault = OptionsUIManager.restoreDefault();
		
		OptionsLanguagesList.setPreferredSize(new Dimension(359, 25));
		OptionsDatasourcesList.setPreferredSize(new Dimension(359, 25));
		OptionsOutputFormatField.setPreferredSize(new Dimension(448, 27));
		OptionsDataList.setPreferredSize(new Dimension(100, 25));
		OptionsAddData.setPreferredSize(new Dimension(80, 25));
		OptionsRemoveData.setPreferredSize(new Dimension(80, 25));
		OptionsSaveAsDefault.setPreferredSize(new Dimension(359, 25));
		OptionsLoadDefault.setPreferredSize(new Dimension(359, 25));
		
		Panel.add(OptionsLanguagesList);
		Panel.add(OptionsDatasourcesList);
		Panel.add(OptionsOutputFormatField);
		Panel.add(OptionsDataList);
		Panel.add(OptionsAddData);
		Panel.add(OptionsRemoveData);
		Panel.add(OptionsSaveAsDefault);
		Panel.add(OptionsLoadDefault);
		
		//Panel.setPreferredSize(new Dimension(700, 300));
		
		return Panel;
		
	}
	
//	private static JPanel panelOptionsAdvanced() {
//		return null;
//	}
	
	private static JPanel panelLogMain() {
		
		JPanel Panel = new JPanel();
		
		LogTableContainer = new JScrollPane(LogUIManager.logTable());
		LogTableContainer.setPreferredSize(new Dimension(740, 509));
		
		Panel.add(LogTableContainer);
		
		return Panel;
		
	}
	
	private static void bindListeners() {
		
		MainTabbedPane.addChangeListener(new listeners.TabbedPane(LogUIManager));
		
		RenamerMediaList.addActionListener(new listeners.RenamerMediaList());
		RenamerChooseDirectory.addActionListener(new listeners.RenamerChooseDirectory());
		RenamerScanDirectory.addActionListener(new listeners.RenamerScanDirectory());
		RenamerCheckErrors.addActionListener(new listeners.RenamerCheckErrors());
		RenamerRenameSelected.addActionListener(new listeners.RenamerRenameSelected());
		OptionsMediaList.addActionListener(new listeners.OptionsMediaList());
		OptionsLanguagesList.addActionListener(new listeners.OptionsLanguagesList());
		OptionsDatasourcesList.addActionListener(new listeners.OptionsDatasourcesList());
		OptionsDataList.addActionListener(new listeners.OptionsDataList());
		OptionsAddData.addActionListener(new listeners.OptionsAddData());
		OptionsRemoveData.addActionListener(new listeners.OptionsRemoveData());
		OptionsSaveAsDefault.addActionListener(new listeners.OptionsSaveAsDefault());
		OptionsLoadDefault.addActionListener(new listeners.OptionsLoadDefault());
		
	}

}