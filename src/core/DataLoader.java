package core;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Get data from APIs.
 * APIs URLs are specified in the XML configuration file.
 * 
 * @see resources/rules.xml
 * 
 * @author evuez
 *
 */
public class DataLoader {
	
	public static void main(String[] args) {
		
		DataLoader dl = new DataLoader();
		HashMap<String, String> Required = new HashMap<String, String>();
		Required.put("title", "cavity job");
		Required.put("artist", "autechre");
		String fURI = dl.makeFinalURI("http://ws.audioscrobbler.com/2.0/?method=track.getinfo&format=json&api_key=b25b959554ed76058ac220b7b2e0a026&artist={artist}&track={title}", Required);
		dl.retrieveDataStack(fURI);
		
	}

	/**
	 * Make a final URI to call a specified API.
	 * 
	 * @param URI
	 * 			API base URI.
	 * @param Data
	 * @return Callable URI to specified API to get necessary data.
	 */
	public String makeFinalURI(String URI, HashMap<String, String> Data) {
		
		Pattern ReplacementPattern = Pattern.compile("\\{(.*?)\\}");
		Matcher URIMatcher = ReplacementPattern.matcher(URI);
		
		StringBuffer URIBuffer = new StringBuffer();
		
		String RequiredValue;
		while (URIMatcher.find()) {
			try {
				RequiredValue = URLEncoder.encode(Data.get(URIMatcher.group(1)).trim(), "UTF-8"); // Encode values to be URL-friendly
			} catch (NullPointerException e) {
				RequiredValue = new String();
			} catch (UnsupportedEncodingException e) {
				RequiredValue = Data.get(URIMatcher.group(1)).trim();
			}
			URIMatcher.appendReplacement(URIBuffer, RequiredValue);
		}
		
		URIMatcher.appendTail(URIBuffer);
		
		return URIBuffer.toString();

	}
	
	/**
	 * Retrieve API returned data.
	 * API is called via the returned URI of core.DataLoader.makeFinalURI
	 * 
	 * @param FinalURI
	 * 			URI from core.DataLoader.makeFinalURI
	 * @return
	 * 
	 * @see #makeFinalURI(String URI, HashMap<String, String> Data)
	 */
	public String retrieveDataStack(String FinalURI) { // TODO check Internet connection
		
		String DataStack = new String();
		
		try {
			URL URI = new URL(FinalURI);
			
			BufferedReader DataStream = new BufferedReader(new InputStreamReader(URI.openStream()));
			String JSONString;
			while ((JSONString = DataStream.readLine()) != null)
				DataStack += JSONString;
			
			DataStream.close();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return DataStack;
		
	}
	
}
