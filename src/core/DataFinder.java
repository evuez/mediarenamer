package core;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.lang.String;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.xpath.*;

// only manage the REALLY "finder" thing (meaning retrieving data from API)
public class DataFinder {
	
	private String PathToRules = "resources/Rules.xml";

	public HashMap<String, String> getFinalData(ArrayList<String> OutputData, String URI, String MovieName) {
		
		HashMap<String, String> FinalData = new HashMap<String, String>();
		
		String StrMovieData = new String(); // JSON String returned from server
		JSONParser parser = new JSONParser();
		KeyFinder finder = new KeyFinder();
		
		// avoid EOF exception
		MovieName = MovieName.replaceAll("([\\.\\s_])", "+"); // avoid EOF exception
		MovieName = MovieName.replaceAll("(?i)" +
							"(dvdrip" +
							"|hdtv" +
							"|xvid" +
							"|subforced" +
							"|vfstfr" +
							"|fastsub" +
							"|brrip" +
							"|ac3" +
							"|dvd)", "");
		MovieName = MovieName.replaceAll("(?i)vost[a-zA-Z]{2}", "");
		//MovieName = MovieName.replaceAll("(?i)(fr|en)", ""); // "freak" = "eak"
		MovieName = MovieName.replaceAll("\\[(.*)\\]", "");
		MovieName = MovieName.replaceAll("\\((.*)\\)", "");
		MovieName = MovieName.replaceAll("[\\+]+", "+");

		try {
			// Create URL for the API
			URL url = new URL(URI+MovieName);
			
			// Get JSON from server
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			String JSONString;
			while ((JSONString = in.readLine()) != null) {
				StrMovieData += JSONString;
			}
			in.close();
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}

		for (String Data : OutputData) {
		
			finder.setMatchKey(Data);

			try{
				parser.parse(StrMovieData, finder, true);
				if(finder.isFound()){
					finder.setFound(false);
					FinalData.put(Data, finder.getValue().toString().trim());
				}
				parser.reset();
			}
			catch(ParseException pe){// TODO add an error for Unavailable service
				return null;
			}
		
		}
			
		return FinalData;
		
	}
	
	public ArrayList<String> getOutputData(String OutputFormat, HashMap<String, String> DataMatch) {
		
		String[] outputdata = OutputFormat.split("<");
		ArrayList<String> OutputData = new ArrayList<String>();

		for (String Data : outputdata) {
			if (Data != null && Data.length() > 0)
				OutputData.add(DataMatch.get(Data.split(">")[0]));
		}
		
		return OutputData;

	}
	
	public String getFinalName(HashMap<String, String> DataMatch, HashMap<String, String> FinalData, String OutputFormat) {
		
		HashMap<String, String> FinalMap = new HashMap<String, String>();
		
		Iterator<Map.Entry<String, String>> DataMatchIterator = DataMatch.entrySet().iterator();
		
		while (DataMatchIterator.hasNext()) {

			Map.Entry<String, String> datamatch = (Map.Entry<String, String>) DataMatchIterator.next();
			
			if (FinalData.get(datamatch.getValue()) != null)
				FinalMap.put(datamatch.getKey(), FinalData.get(datamatch.getValue()));
				
		}
	
		Scanner scanOutputFormat = new Scanner(OutputFormat);
		scanOutputFormat.useDelimiter("<|>");
		String FinalName = OutputFormat;
		while (scanOutputFormat.hasNext()) {
		    final String data = scanOutputFormat.next();
		    final String finalname = FinalMap.get(data);
		    if (finalname != null) {
		    	FinalName = FinalName.replace("<" + data + ">", finalname);
		    }
		}
		
		// return null if nothing replaced
		
		if (FinalName != OutputFormat)
			return FinalName;
		
		return null;
		
	}

	public static Document initRulesForAPI(String linkToRulesForAPI) {
		
		try {
			File RulesForAPI = new File(linkToRulesForAPI);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document rules = dBuilder.parse(RulesForAPI);
			rules.getDocumentElement().normalize();
			return rules;
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;

	}
	
	private static Object getXPathObject(String DOM, String XPath) {
		
		Object XPathObject = new Object();
		
		Document rules = initRulesForAPI(DOM);
		
		try {

			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
			XPathExpression expr = xpath.compile(XPath);

			XPathObject = expr.evaluate(rules, XPathConstants.NODESET);

		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return XPathObject;
		
	}
	
	public String getURI(String language, String datasource) {
		
		Object XPathObject = getXPathObject(PathToRules, "//language[@value='"+language+"']/api[@name='"+datasource+"']/@uri");
		NodeList uri = (NodeList) XPathObject;
		String URI = uri.item(0).getTextContent();
		
		return URI;
		
	}
	
	public HashMap<String, Boolean> getRequired() {
		
		Object XPathObject = getXPathObject(PathToRules, "//required//data");
		NodeList required = (NodeList) XPathObject;
		
		HashMap<String, Boolean> Required = new HashMap<String, Boolean>();
		
		for (int i = 0; i < required.getLength(); i++) {
			Element data = (Element) required.item(i);
			Required.put(data.getAttribute("name"), Boolean.parseBoolean(data.getAttribute("selected")));
		}
			
		return Required;
		
	}
	
	public HashMap<String, String> getDataMatch(String language, String datasource) {
		
		Object XPathObject = getXPathObject(PathToRules, "//language[@value='"+language+"']/api[@name='"+datasource+"']/datamatch");
		NodeList datamatch = (NodeList) XPathObject;
		
		HashMap<String, String> DataMatch = new HashMap<String, String>();
		
		for (int i = 0; i < datamatch.getLength(); i++) {
			Element data = (Element) datamatch.item(i);
			DataMatch.put(data.getAttribute("type"), data.getTextContent());
		}
		
		return DataMatch;

	}

}