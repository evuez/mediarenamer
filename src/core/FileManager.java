package core;

import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.swing.JFileChooser;

/**
 * Manage all file-related methods
 * 
 * @author evuez
 *
 */
public class FileManager {
	
	/**
	 * Allows to get a filename without its extension.
	 * 
	 * @param FileName
	 * 			Name of a file with its extension.
	 * @return Given filename without its extension.
	 */
	public String getFileNameWithoutExtension(String FileName) {
		
        File tmpFile = new File(FileName);
        tmpFile.getName();
        int whereDot = tmpFile.getName().lastIndexOf('.');
        if (0 < whereDot && whereDot <= tmpFile.getName().length() - 2 ) {
            return tmpFile.getName().substring(0, whereDot);
        }    
        return null;
        
    }
	
	/**
	 * Allows to get the extension of a file.
	 * 
	 * @param FileName
	 * 			Name of a file with its extension.
	 * @param KeepDot
	 * 			Whether you want to keep or not the dot in the extension.
	 * @return Extension with or without dot of given filename.
	 */
	public String getExtension(String FileName, Boolean KeepDot) {
		Integer keepdot = (!KeepDot)?1:0;
		return FileName.substring(FileName.lastIndexOf(".")+keepdot, FileName.length());
	}
	
	/**
	 * Sanitize filename to avoid issues in file systems.
	 * 
	 * @param FileName
	 * 			Name of a file.
	 * @return
	 * 			Sanitized filename.
	 */
	public String sanitizeFileName(String FileName) {
		
		String SanitizedFileName = null;
		if (FileName != null)
			SanitizedFileName = FileName.replaceAll("[:\\\\/*?|<>\"]", "");
		return SanitizedFileName;
		
	}
	
	/**
	 * Get the number of file in a given directory.
	 * 
	 * @param Directory
	 * 			Path to a directory.
	 * @return
	 * 			Number of file in the given directory.
	 */
	public int getNumberOfFiles(String Directory) {
		
		File FilesDirectory = new File(Directory);
		int NumberOfFiles = 0;
		
		for (File File : FilesDirectory.listFiles()) {
			if (File.isFile())
				NumberOfFiles++;
		}
		
		return NumberOfFiles;
		 
	}
	
	/**
	 * Avoid special characters to be mishandled.
	 * @param StringToDecode
	 * 			String with special characters.
	 * @return Beautiful utf-8 string.
	 */
	public String utf8Decode(String StringToDecode) {
		
		String UTF8Decoded = null;
		
		if (StringToDecode != null) {
			try {
				UTF8Decoded = new String(StringToDecode.getBytes("ISO-8859-1"),"UTF-8");
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		}
		
		return UTF8Decoded;
		
	}
	
	/**
	 * Allows to select a directory in a GUI environment.
	 * 
	 * @return Path of the selected directory.
	 */
	public String chooseDirectory() {
		
		JFileChooser DirectoryChooser = new JFileChooser();
		DirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		DirectoryChooser.setAcceptAllFileFilterUsed(false);
		
		if (DirectoryChooser.showOpenDialog(gui.MediaRenamer.MediaRenamerGUI) == JFileChooser.APPROVE_OPTION) { 
			return DirectoryChooser.getSelectedFile().toString();
		} else {
			return new String();
		}

	}

}
