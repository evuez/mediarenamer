package uimanagers;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Log implements main.Settings {
	
	private String LogSeparator = "          ";
	private String LogExtension = ".log";
	private String LogName;
	
	public DefaultTableModel LogTableModel;
	
	public static void main(String args[]) {
		Log el = new Log();
		el.logTable();
		el.updateLog("resources/logs/", "CannotRename", new String[]{"Super movie"});
		el.updateLogTable("resources/logs/");
	}
	
	public Log() {
		DateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd");
		LogName = DateFormat.format(new Date());
		File LogFile = new File(LogPath+LogName+LogExtension);
		if (!LogFile.exists()) {
			try {
				LogFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace(); // TODO alert "Cannot load log file"
			}
		}
	}
	
	public JTable logTable() {
		
		Object[][] LogTableCells = new Object[0][0];
		String[] LogTableHead = {"Date", "Notice"};
		LogTableModel = new DefaultTableModel(LogTableCells, LogTableHead);
		
		JTable LogTable = new JTable(LogTableModel);
		
		LogTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		LogTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		LogTable.getColumnModel().getColumn(0).setPreferredWidth(60);
		LogTable.getColumnModel().getColumn(1).setPreferredWidth(530);
		
		return LogTable;
		
	}
	
	/** 
	 * Update Specified Log File
	 * Must be done to go further with UpdateLogTable
	 * @param String LogPath
	 * @param String NoticeId
	 * @param String[] NoticeArgs
	 */
	public void updateLog(String LogPath, String NoticeId, String[] NoticeArgs) {
		
		String XPathNotice = "//notices/notice[@id='"+NoticeId+"']";
		Object XPathObject = RulesManager.xpathObject(XPathNotice);
		String lognotice = ((Node) ((NodeList) XPathObject).item(0)).getTextContent();
		String LogNotice = new String();
		
		if (NoticeArgs.length != 0) {
			Pattern ReplacementPattern = Pattern.compile("\\{(.*?)\\}");
			Matcher ArgumentMatcher = ReplacementPattern.matcher(lognotice);
			StringBuffer NoticeBuffer = new StringBuffer();
			while (ArgumentMatcher.find())
				ArgumentMatcher.appendReplacement(NoticeBuffer, NoticeArgs[Integer.parseInt(ArgumentMatcher.group(1))]);
			ArgumentMatcher.appendTail(NoticeBuffer);
			LogNotice = NoticeBuffer.toString();
		} else {
			LogNotice = lognotice;
		}
		
		DateFormat DateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date Date = new Date();
		 
		try {
			FileWriter LogFile = new FileWriter(LogPath+LogName+LogExtension, true);
			BufferedWriter LogStream = new BufferedWriter(LogFile);
			
			LogStream.write(DateFormat.format(Date));
			LogStream.write(LogSeparator);
			LogStream.write(LogNotice);
			LogStream.newLine();
			
			LogStream.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Update Log Table With Specified Log File
	 * UpdateLog must be called to get new logs
	 * @param String LogPath
	 */
	public void updateLogTable(String LogPath) {
		
		// Empty Table Model before updating
		LogTableModel.setRowCount(0);
		
		try {
			FileInputStream LogFile = new FileInputStream(LogPath+LogName+LogExtension);
			DataInputStream LogStream = new DataInputStream(LogFile);
			BufferedReader LogStreamReader = new BufferedReader(new InputStreamReader(LogStream));
			String LogLine;
			
			while ((LogLine = LogStreamReader.readLine()) != null) {
				String[] LogLineTokens = LogLine.split(LogSeparator);
				LogTableModel.insertRow(0, new Object[]{LogLineTokens[0], LogLineTokens[1]});
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
}
