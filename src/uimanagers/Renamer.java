package uimanagers;


import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import org.w3c.dom.NodeList;

public class Renamer implements main.Settings {
	
	public JComboBox mediaList() {
		
		String XPathMedia = "//media/@type";
		
		Object XPathObject = RulesManager.xpathObject(XPathMedia);
		NodeList media = (NodeList) XPathObject;

		String[] Media = new String[media.getLength()];
		for (int i = 0; i < media.getLength(); i++)
			Media[i] = media.item(i).getTextContent();
		
		return new JComboBox(Media);
		
	}
	
	public JTextField directoryField() {
		
		JTextField DirectoryField = new JTextField();
		DirectoryField.setEnabled(false);

		return DirectoryField;
		
	}
	
	public JButton chooseDirectory() {
		return new JButton("Choose a directory to scan");
	}
	
	public JButton scanDirectory() {
		
		JButton ScanDirectory = new JButton("Scan directory");
		ScanDirectory.setEnabled(false);
		
		return ScanDirectory;
		
	}
	
	public JButton checkErrors() {
		
		JButton CheckErrors = new JButton("Check for errors");
		CheckErrors.setEnabled(false);
		
		return CheckErrors;
		
	}
	
	public JButton renameSelected() {
		
		JButton RenameSelected = new JButton("Rename selected files");
		RenameSelected.setEnabled(false);
		
		return RenameSelected;
		
	}
	
	public DefaultTableModel dataTableModel() {
		
		Object[][] DataTableCells = new Object[0][0];
		String[] DataTableHead = {"", "filename", "new filename", "extension"};
		DefaultTableModel DataTableModel = new DefaultTableModel(DataTableCells, DataTableHead);
		
		return DataTableModel;
		
	}

	public JTable dataTable(DefaultTableModel dataTableModel) {

		JTable DataTable = new JTable(dataTableModel) {

			private static final long serialVersionUID = 1L;

			@Override
			public Class<?> getColumnClass(int c) {
				switch (c) {
					case 0:
						return Boolean.class;
					default:
						return String.class;
				}
			}
			
			@Override
			public boolean isCellEditable(int rowIndex, int columnIndex){
				switch (columnIndex) {
					case 0:
					case 2:
						return true;
					default:
						return false;
				}

			}

		};
		
		DataTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		DataTable.setAutoResizeMode(JTable.AUTO_RESIZE_NEXT_COLUMN);
		
		DataTable.getColumnModel().getColumn(0).setPreferredWidth(10);
		DataTable.getColumnModel().getColumn(1).setPreferredWidth(300);
		DataTable.getColumnModel().getColumn(2).setPreferredWidth(350);
		DataTable.getColumnModel().getColumn(3).setPreferredWidth(65);
		
		return DataTable;
		
	}
	
	public JProgressBar mainProcess() {
		
		JProgressBar ScanProcess = new JProgressBar();
		ScanProcess.setMinimum(0);
		ScanProcess.setStringPainted(true);
		ScanProcess.setString("");
		
		return ScanProcess;
		
	}
	
}
