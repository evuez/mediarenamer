package uimanagers;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JTextField;

import org.w3c.dom.NodeList;
// TODO add Default-thing implementation--need to set default media, default language, default datasource
// TODO Option for off-line storage to set here (manage away, special class for Database)
public class Options implements main.Settings {

	public JComboBox mediaList() { // + int Width, int Height
		
		String XPathMedia = "//media/@type";
		
		Object XPathObject = RulesManager.xpathObject(XPathMedia);
		NodeList media = (NodeList) XPathObject;

		String[] Media = new String[media.getLength()];
		for (int i = 0; i < media.getLength(); i++)
			Media[i] = media.item(i).getTextContent();
		
		return new JComboBox(Media);
		
	}

	public JComboBox languagesList(String Media) {
		
		String XPathLanguages = "//media[@type='"+Media+"']/datasources/language/@name";
		
		Object XPathObject = RulesManager.xpathObject(XPathLanguages);
		NodeList languages = (NodeList) XPathObject;

		String[] Languages = new String[languages.getLength()];
		for (int i = 0; i < languages.getLength(); i++)
			Languages[i] = languages.item(i).getTextContent();
		
		return new JComboBox(Languages);
		
	}
	
	public JComboBox datasourcesList(String Media, String Language) {
		
		String XPathDatasources = "//media[@type='"+Media+"']/datasources/language[@name='"+Language+"']/api/@name";
		
		Object XPathObject = RulesManager.xpathObject(XPathDatasources);
		NodeList datasources = (NodeList) XPathObject;

		String[] Datasources = new String[datasources.getLength()];
		for (int i = 0; i < datasources.getLength(); i++)
			Datasources[i] = datasources.item(i).getTextContent();
		
		return new JComboBox(Datasources);
		
	}
	
	public JTextField outputFormatField() {
		return new JTextField();
	}
	
	public JComboBox dataList(String Media, String Language, String Datasource) {
		
		String XPathData = "//media[@type='"+Media+"']/datasources/language[@name='"+Language+"']/api[@name='"+Datasource+"']/datamatch/@type";
		
		Object XPathObject = RulesManager.xpathObject(XPathData);
		NodeList data = (NodeList) XPathObject;

		String[] Data = new String[data.getLength()];
		for (int i = 0; i < data.getLength(); i++)
			Data[i] = data.item(i).getTextContent();
		
		return new JComboBox(Data);

	}
	
	public JButton addData() {
		return new JButton("Add");
	}
	
	public JButton removeData() {
		return new JButton("Remove");
	}
	
	public JButton saveAsDefault() {
		return new JButton("Save as default");
	}
	
	public JButton restoreDefault() {
		return new JButton("Load default");
	}
	
	/* TODO
	 * Adding a default-moving-to directory
	 * - JCheckbox: "Move renamed files to the following folder" JTextField, JButton(ChooseDirectory)
	 * - JCheckbox: "Move renamed files to the following folder" JButton(ChooseDirectory/ChosenDirectory)
	 */
	
	
}
