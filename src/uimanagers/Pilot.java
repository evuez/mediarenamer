package uimanagers;

import java.awt.Color;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import javax.swing.JTabbedPane;

public class Pilot {
	
	private ScheduledExecutorService Scheduler;
	private JTabbedPane Tabs;
	private Color SavedColor;
	private Color BlinkColor;
	private boolean BlinkFlag;
	private boolean PendingWarnProcess;
	
	public Pilot(JTabbedPane tabs, Color color) {
		Tabs = tabs;
		SavedColor = Tabs.getBackground();
		BlinkColor = color;
	}
	
	public void warn(int index, boolean warn) {
		
		final int TabIndex = index;
		
		if (warn && !PendingWarnProcess) {
			
			PendingWarnProcess = true;
			
			Scheduler = Executors.newScheduledThreadPool(1);
			
			final Runnable Warn = new Runnable() {
				public void run() {
					if (BlinkFlag) {
						Tabs.setBackgroundAt(TabIndex, SavedColor);
						BlinkFlag = !BlinkFlag;
					} else {
						Tabs.setBackgroundAt(TabIndex, BlinkColor);
						BlinkFlag = !BlinkFlag;
					}
				}
			};
			
			final ScheduledFuture<?> WarningHandle = 
				Scheduler.scheduleAtFixedRate(Warn, 500, 500, TimeUnit.MILLISECONDS);
			
			Scheduler.schedule(new Runnable() {
				public void run() {
					WarningHandle.cancel(true);
				}
			}, 60, TimeUnit.MINUTES);
			
		} else if (!warn) {
			try {
				Scheduler.shutdownNow();
				PendingWarnProcess = false;
				Tabs.setBackgroundAt(TabIndex, SavedColor);
			} catch (NullPointerException ignored) {}
		}
		
	}

}
