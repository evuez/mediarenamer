package handlers;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Media implements main.Settings {
	
	private String Media;
	private String Language;
	private String Datasource;
	private core.DataLoader DataLoader = new core.DataLoader();
	private core.KeyFinder KeyFinder = new core.KeyFinder();
	private core.FileManager FileManager = new core.FileManager();
	private static uimanagers.Log LogUIManager = new uimanagers.Log();
	private JSONParser JSONParser = new JSONParser();

	
	public static void main(String args[]) {
		Media media = new Media("Music", "Default", "LastFM");
		String[] methods = media.setParseMethod();
		HashMap<String, String> ParsedData = media.getData(methods, "D:\\TESTING\\musics\\Set The Controls For The Heart Of The Sun Pink Floyd.mp3");
		String DataStack = media.getDataStack(ParsedData);
		HashMap<String, String> datastack = media.matchDataStack(DataStack);
		String newname = media.makeNewName("<artist> - <title>", datastack);
		media.renameFile("D:\\TESTING\\musics\\Set The Controls For The Heart Of The Sun Pink Floyd.mp3", newname);
//		Media media = new Media("Movie");
//		String[] methods = media.SetParseMethod();
//		HashMap<String, String> ParsedData = media.GetData(methods, "D:\\supertruc\\Shaun.Of.The.Dead.FRENCH.SUBFORCED.DVDRiP.XViD.CBR-ALEI.avi");
//		String DataStack = media.GetDataStack("IMDB", "English", ParsedData);
//		System.out.println(media.MatchDataStack("IMDB", "English", DataStack));
	}
	
	public Media(String media, String language, String datasource) {
		Media = media;
		Language = language;
		Datasource = datasource;
	}
	
	public String[] setParseMethod() { // TODO should be GetParseMethod(s)
		
		String XPathParse = SetupXPath+"[@media='"+Media+"' and @type='parse']";
		
		Object XPathObject = RulesManager.xpathObject(XPathParse);
		Node ParseNode = (Node) ((NodeList) XPathObject).item(0);
		
		String Parse = ParseNode.getTextContent();
		
		String[] ParseMethods = Parse.split(",");
		
		return ParseMethods;
		
	}
	
	@SuppressWarnings("unchecked")
	public HashMap<String, String> getData(String[] ParseMethods, String FilePath) {
		
		HashMap<String, String> ParsedData = new HashMap<String, String>();
		Method Parser;
		
		for (String ParseMethod : ParseMethods) {
			try {
				
				Class<?> ParserClass = Class.forName("modules."+ParseMethod);
				Object ParserObject = ParserClass.newInstance();
				Parser = ParserClass.getMethod("parseFile", String.class, String.class);
				ParsedData.putAll((HashMap<String, String>) Parser.invoke(ParserObject, Media, FilePath));
				
			} catch (ClassNotFoundException e) {
				return null; // TODO to return filename
			} catch (NoSuchMethodException e) {
				return null; // ...
			} catch (SecurityException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				e.printStackTrace();
			} catch (InstantiationException e) {
				e.printStackTrace();
			}
		}
		
		return ParsedData;
		
	} // Parse file, return  title [and artist,	and needed...]
	
	public String getDataStack(HashMap<String, String> ParsedData){
		
		String XPathURI = "//media[@type='"+Media+"']/datasources/language[@name='"+Language+"']/api[@name='"+Datasource+"']/@uri";
		
		Object XPathObject = RulesManager.xpathObject(XPathURI);
		String URI = ((NodeList) XPathObject).item(0).getTextContent();
		
		String FinalURI = DataLoader.makeFinalURI(URI, ParsedData);

		return DataLoader.retrieveDataStack(FinalURI);
		
	} // Get DataStack from API
	
	public HashMap<String, String> matchDataStack(String DataStack) {
		
		HashMap<String, String> DataStackMap = new HashMap<String, String>();
		
		String XPathDataMatch = "//media[@type='"+Media+"']/datasources/language[@name='"+Language+"']/api[@name='"+Datasource+"']/datamatch";
		
		Object XPathObject = RulesManager.xpathObject(XPathDataMatch);
		NodeList datamatch = (NodeList) XPathObject;

		for (int i = 0; i < datamatch.getLength(); i++) {
			
			Element data = (Element) datamatch.item(i);
			
			KeyFinder.setMatchKey(data.getTextContent());

			try {
				
				JSONParser.parse(DataStack, KeyFinder, true);
				
				if (KeyFinder.isFound()) {
					KeyFinder.setFound(false);
					try {
						DataStackMap.put(data.getAttribute("type"), KeyFinder.getValue().toString().trim());
					} catch (NullPointerException e) {
						LogUIManager.updateLog(LogPath, "MissingData", new String[]{});
					}
				}
				
				JSONParser.reset();
				
			}
			catch (ParseException pe) {
				return null;
			}
			
		}
		
		return DataStackMap;
		
	} // Match DataStack w/ Rules-DataStack

	public String makeNewName(String OutputFormat, HashMap<String, String> DataStack) {
		
		Boolean NewNameCreated = true;
		String NewName = new String();
		
		Pattern TagsPattern = Pattern.compile("<([^>]*)>");
		Matcher TagsMatcher = TagsPattern.matcher(OutputFormat);
		
		while (TagsMatcher.find()) {
			try {
				String TagValue = DataStack.get(TagsMatcher.group(1));
				OutputFormat = OutputFormat.replace(TagsMatcher.group(0), TagValue);
			} catch (NullPointerException npe) {
				NewNameCreated = false;
			}
		}

		if (NewNameCreated == true) {
			NewName = FileManager.sanitizeFileName(OutputFormat);
		}
		
		return NewName;
		
	} // Create new filename
	
	public boolean renameFile(String OldPath, String NewName) {
		
		NewName = FileManager.sanitizeFileName(NewName);
		
		File OldFile = new File(OldPath);
		String Path = OldFile.getParent()+File.separator;
		String Extension = FileManager.getExtension(OldPath, true);
		File NewFile = new File(Path+NewName+Extension);
		
		boolean Renamed = OldFile.renameTo(NewFile);
		
		return Renamed;
		
	} // Rename file
	
}