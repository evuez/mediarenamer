package modules;

import java.util.HashMap;

public abstract class Parsers {

	public HashMap<String, String> ParsedData = new HashMap<String, String>();
	
	public abstract HashMap<String, String> parseFile(String Media, String Name);
	
}
