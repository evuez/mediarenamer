package modules;

import java.io.File;
import java.util.HashMap;

import org.w3c.dom.Attr;
import org.w3c.dom.NodeList;

public class Trash extends Parsers implements main.Settings {
	
	private core.FileManager FileManager = new core.FileManager();
	
	public static void main(String[] args) {
		Trash t = new Trash();
		t.parseFile("Movie", "D:\\supertruc\\Territoires.2011.FRENCH.DVDRIP.XVID-LEGiON.avi");
	}
	
	public HashMap<String, String> parseFile(String Media, String FilePath) {
		
		String Name = FileManager.getFileNameWithoutExtension(new File(FilePath).getName());
		
		String XPathTrash = "//media[@type='"+Media+"']/trash/needle/@*";
		
		Object XPathObject = RulesManager.xpathObject(XPathTrash);
		NodeList needles = (NodeList) XPathObject;
		
		for (int i = 0; i < needles.getLength(); i += 2) { // increment by 2, to get Pattern and Replacement at once
			
			Attr NeedlePattern = (Attr) needles.item(i);
			Attr NeedleReplacement = (Attr) needles.item(i+1);
			
			Name = Name.replaceAll(NeedlePattern.getValue(), NeedleReplacement.getValue());
			
		}
		
		ParsedData.put("title", Name);

		return ParsedData;
	}

}
