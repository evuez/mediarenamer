package modules;

import java.io.File;
import java.util.HashMap;

import org.cmc.music.metadata.IMusicMetadata;
import org.cmc.music.myid3.MyID3;

public class ID3 extends Parsers {
	
	public static void main(String[] args) {
		ID3 i = new ID3();
		System.out.println(i.parseFile(null, "D:\\documents\\Downloads\\Musics\\Pink Floyd\\Atom Heart Mother.mp3"));
	}

	public HashMap<String, String> parseFile(String Media, String FilePath) {
		
		File File = new File(FilePath);
		
		try {
			IMusicMetadata Tags = new MyID3().read(File).getSimplified();
			
			ParsedData.put("title", Tags.getSongTitle());
			ParsedData.put("artist", Tags.getArtist());
			
		} catch (Exception e) {}
		
		return ParsedData;
	}
	
}
