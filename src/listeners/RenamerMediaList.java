package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class RenamerMediaList implements ActionListener {

	public void actionPerformed(ActionEvent e) {
		
		gui.MediaRenamer.RenamerScanDirectory.setEnabled(false);
		gui.MediaRenamer.RenamerCheckErrors.setEnabled(false);
		gui.MediaRenamer.RenamerRenameSelected.setEnabled(false);
		
	}

}
