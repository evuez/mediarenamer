package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JTextField;

public class OptionsLoadDefault implements main.Settings, ActionListener {

	private JComboBox LanguagesList;
	private JComboBox DatasourcesList;
	private JTextField OutputFormatField;
	private String SelectedMedia;

	public void actionPerformed(ActionEvent e) {
		
		SelectedMedia = (String) gui.MediaRenamer.OptionsMediaList.getSelectedItem();
		
		LanguagesList = gui.MediaRenamer.OptionsLanguagesList;
		DatasourcesList = gui.MediaRenamer.OptionsDatasourcesList;
		OutputFormatField = gui.MediaRenamer.OptionsOutputFormatField;

		LanguagesList.setSelectedItem((Object) RulesManager.loadDefault(SelectedMedia, "language"));
		DatasourcesList.setSelectedItem((Object) RulesManager.loadDefault(SelectedMedia, "datasource"));
		OutputFormatField.setText(RulesManager.loadDefault(SelectedMedia, "outputformat"));
		
	}

}
