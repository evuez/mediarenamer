package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OptionsAddData implements ActionListener {
	
	private String SelectedData;
	private String CurrentOutputFormat;

	public void actionPerformed(ActionEvent e) {

		SelectedData = (String) gui.MediaRenamer.OptionsDataList.getSelectedItem();
		SelectedData = "<"+SelectedData+">";
		
		CurrentOutputFormat = gui.MediaRenamer.OptionsOutputFormatField.getText();
		
		gui.MediaRenamer.OptionsOutputFormatField.setText(CurrentOutputFormat + SelectedData);
		
	}

}
