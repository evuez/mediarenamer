package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import uimanagers.Options;


public class OptionsMediaList implements ActionListener {
	
	private Options OptionsUIManager = new Options();
	private String SelectedMedia;
	private JComboBox LanguagesList;
	private int NumberOfItems;

	public void actionPerformed(ActionEvent e) {
		
		gui.MediaRenamer.OptionsLanguagesList.removeAllItems();
		
		SelectedMedia = (String) gui.MediaRenamer.OptionsMediaList.getSelectedItem();
		LanguagesList = OptionsUIManager.languagesList(SelectedMedia);

		NumberOfItems = LanguagesList.getItemCount();
		for (int i = 0; i < NumberOfItems; i++)
			gui.MediaRenamer.OptionsLanguagesList.addItem(LanguagesList.getItemAt(i));
		
		gui.MediaRenamer.OptionsLoadDefault.doClick();
		
	}

}
