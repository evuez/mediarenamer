package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import uimanagers.Options;


public class OptionsLanguagesList implements ActionListener {
	
	private Options OptionsUIManager = new Options();
	private String SelectedMedia;
	private String SelectedLanguage;
	private JComboBox DatasourcesList;
	private int NumberOfItems;

	public void actionPerformed(ActionEvent e) {
		
		gui.MediaRenamer.OptionsDatasourcesList.removeAllItems();
		
		SelectedMedia = (String) gui.MediaRenamer.OptionsMediaList.getSelectedItem();
		SelectedLanguage = (String) gui.MediaRenamer.OptionsLanguagesList.getSelectedItem();
		DatasourcesList = OptionsUIManager.datasourcesList(SelectedMedia, SelectedLanguage);

		NumberOfItems = DatasourcesList.getItemCount();
		for (int i = 0; i < NumberOfItems; i++)
			gui.MediaRenamer.OptionsDatasourcesList.addItem(DatasourcesList.getItemAt(i));
	}

}
