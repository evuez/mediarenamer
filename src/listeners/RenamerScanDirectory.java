package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.HashMap;

import javax.swing.table.DefaultTableModel;

public class RenamerScanDirectory implements ActionListener, main.Settings {
	
	private static uimanagers.Log LogUIManager = new uimanagers.Log();
	private core.FileManager FileManager = new core.FileManager();
	private uimanagers.Pilot PilotUIManager = gui.MediaRenamer.PilotUIManager;
	private DefaultTableModel DataTableModel = gui.MediaRenamer.DataTableModel;

	private String DirectoryPath;
	private File Directory;
	private Integer NumberOfFiles;
	private Integer NumberOfScannedFiles;

	public void actionPerformed(ActionEvent arg0) {
		
		Directory = new File(gui.MediaRenamer.RenamerDirectoryField.getText());
		DirectoryPath = Directory.getAbsolutePath()+File.separator;
		NumberOfFiles = FileManager.getNumberOfFiles(DirectoryPath);
		NumberOfScannedFiles = 0;
		
		LogUIManager.updateLog(LogPath, "NewScan", new String[]{});
		
		if (!Directory.exists() || NumberOfFiles < 1) {
			
			LogUIManager.updateLog(LogPath, "NullDirectory", new String[]{});
			PilotUIManager.warn(2, true);
			
		} else {
			
			gui.MediaRenamer.RenamerScanDirectory.setEnabled(false);
			gui.MediaRenamer.RenamerCheckErrors.setEnabled(false);
			gui.MediaRenamer.RenamerRenameSelected.setEnabled(false);
			
			gui.MediaRenamer.RenamerScanProcess.setMaximum(NumberOfFiles);
			gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+NumberOfFiles.toString());
			
			
			DataTableModel.setRowCount(0);
			
			final File[] FileList = Directory.listFiles();
			
			for (File GivenFile : FileList) {
				
				if (GivenFile.isFile()) {
					
					NumberOfScannedFiles++;
					
					String FileName = GivenFile.getName();
					String Name = FileManager.getFileNameWithoutExtension(FileName);
					String Extension = FileManager.getExtension(FileName, false);
					
					DataTableModel.insertRow(DataTableModel.getRowCount(), 
							(new Object[]{
									false,
									Name,
									"",
									Extension
							})
					);
					
					gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+NumberOfFiles.toString());
					gui.MediaRenamer.RenamerScanProcess.setValue(NumberOfScannedFiles);
					
				}
				
			}
			
			ScannerThread Scanner = new ScannerThread(DataTableModel, DirectoryPath);
			new Thread(Scanner).start();

		}
		
	}

}

class ScannerThread implements Runnable, main.Settings {
	
	private core.FileManager FileManager = new core.FileManager();
	private handlers.Media Media;
	private DefaultTableModel DataTableModel;
	
	private String SelectedMedia;
	private String SelectedLanguage;
	private String SelectedDatasource;
	private String OutputFormat = gui.MediaRenamer.OptionsOutputFormatField.getText();
	
	private Integer NumberOfScannedFiles;
	private String DirectoryPath;
	
	public ScannerThread(DefaultTableModel datatablemodel, String directorypath) {
		DataTableModel = datatablemodel;
		DirectoryPath = directorypath;
	}
	
	public void run() {
		
		NumberOfScannedFiles = 0;
		gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+DataTableModel.getRowCount());
		gui.MediaRenamer.RenamerScanProcess.setMaximum(DataTableModel.getRowCount());
		

		SelectedMedia = gui.MediaRenamer.RenamerMediaList.getSelectedItem().toString();
		
		if (SelectedMedia != gui.MediaRenamer.OptionsMediaList.getSelectedItem().toString() || OutputFormat.equals("")) {
			SelectedLanguage = RulesManager.loadDefault(SelectedMedia, "language");
			SelectedDatasource = RulesManager.loadDefault(SelectedMedia, "datasource");
			OutputFormat = RulesManager.loadDefault(SelectedMedia, "outputformat");
		} else {
			SelectedLanguage = gui.MediaRenamer.OptionsLanguagesList.getSelectedItem().toString();
			SelectedDatasource = gui.MediaRenamer.OptionsDatasourcesList.getSelectedItem().toString();
		}
		
		Media = new handlers.Media(SelectedMedia, SelectedLanguage, SelectedDatasource);
		String[] ParseMethods = Media.setParseMethod();

	
		for (int row = 0; row < DataTableModel.getRowCount(); row++) {
			
			String Name = (String) DataTableModel.getValueAt(row, 1);
			String Extension = (String) DataTableModel.getValueAt(row, 3);
			
			HashMap<String, String> ParsedData = Media.getData(ParseMethods, DirectoryPath+Name+"."+Extension);
			String DataStack = Media.getDataStack(ParsedData);
			HashMap<String, String> MatchedDataStack = Media.matchDataStack(DataStack);
			String NewName = Media.makeNewName(OutputFormat, MatchedDataStack);
			
			NewName = FileManager.utf8Decode(NewName); // still getting some errors..
			NewName = FileManager.sanitizeFileName(NewName);
			
			if (NewName.equals("") || NewName.equals(Name)) {
				DataTableModel.setValueAt(Name, row, 2);
				DataTableModel.setValueAt(false, row, 0);
			} else {
				DataTableModel.setValueAt(NewName, row, 2);
				DataTableModel.setValueAt(true, row, 0);
			}
			
			NumberOfScannedFiles++;
			
			gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+DataTableModel.getRowCount());
			gui.MediaRenamer.RenamerScanProcess.setValue(NumberOfScannedFiles);
	
		}
		
		gui.MediaRenamer.RenamerScanDirectory.setEnabled(true);
		gui.MediaRenamer.RenamerCheckErrors.setEnabled(true);
		
	}
		
}
