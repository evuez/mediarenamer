package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;

import uimanagers.Options;


public class OptionsDatasourcesList implements ActionListener {
	
	private Options OptionsUIManager = new Options();
	private String SelectedMedia;
	private String SelectedLanguage;
	private String SelectedDatasource;
	private JComboBox DataList;
	private int NumberOfItems;

	public void actionPerformed(ActionEvent e) {
		
		gui.MediaRenamer.OptionsOutputFormatField.setText(null);
		gui.MediaRenamer.OptionsDataList.removeAllItems();
		
		SelectedMedia = (String) gui.MediaRenamer.OptionsMediaList.getSelectedItem();
		SelectedLanguage = (String) gui.MediaRenamer.OptionsLanguagesList.getSelectedItem();
		SelectedDatasource = (String) gui.MediaRenamer.OptionsDatasourcesList.getSelectedItem();
		DataList = OptionsUIManager.dataList(SelectedMedia, SelectedLanguage, SelectedDatasource);
		
		NumberOfItems = DataList.getItemCount();
		for (int i = 0; i < NumberOfItems; i++)
			gui.MediaRenamer.OptionsDataList.addItem(DataList.getItemAt(i));
	}

}
