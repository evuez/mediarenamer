package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import core.FileManager;

public class RenamerChooseDirectory implements ActionListener {
	
	private core.FileManager FileManager = new FileManager();
	
	public void actionPerformed(ActionEvent e) {
		
		String ChosenDirectory = FileManager.chooseDirectory();
		
		if (!ChosenDirectory.equals("")) {
			gui.MediaRenamer.RenamerDirectoryField.setText(ChosenDirectory);
			gui.MediaRenamer.RenamerScanDirectory.setEnabled(true);
		}
		
	}

}
