package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OptionsSaveAsDefault implements main.Settings, ActionListener {
	
	private String SelectedMedia;
	private String SelectedLanguage;
	private String SelectedDatasource;
	private String OutputFormat;

	public void actionPerformed(ActionEvent e) {
		
		SelectedMedia = (String) gui.MediaRenamer.OptionsMediaList.getSelectedItem();
		
		SelectedLanguage = (String) gui.MediaRenamer.OptionsLanguagesList.getSelectedItem();
		SelectedDatasource = (String) gui.MediaRenamer.OptionsDatasourcesList.getSelectedItem();
		OutputFormat = (String) gui.MediaRenamer.OptionsOutputFormatField.getText();
		
		RulesManager.saveAsDefault(SelectedMedia, "language", SelectedLanguage);
		RulesManager.saveAsDefault(SelectedMedia, "datasource", SelectedDatasource);
		RulesManager.saveAsDefault(SelectedMedia, "outputformat", OutputFormat);
		
	}

}
