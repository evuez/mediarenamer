package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OptionsRemoveData implements ActionListener {

	private String SelectedData;
	private String CurrentOutputFormat;
	private String OutputFormat;

	public void actionPerformed(ActionEvent e) {
		
		SelectedData = (String) gui.MediaRenamer.OptionsDataList.getSelectedItem();
		SelectedData = "<"+SelectedData+">";
		
		CurrentOutputFormat = gui.MediaRenamer.OptionsOutputFormatField.getText();
		
		OutputFormat = CurrentOutputFormat.replaceAll(SelectedData, "");
		
		gui.MediaRenamer.OptionsOutputFormatField.setText(OutputFormat);
		
	}
	
}
