package listeners;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class TabbedPane implements main.Settings, ChangeListener {
	
	private uimanagers.Log Log;
	
	public TabbedPane(uimanagers.Log LogUIManager) {
		Log = LogUIManager;
	}
	
	public void stateChanged(ChangeEvent e) {

		JTabbedPane Pane = (JTabbedPane) e.getSource();
		Integer SelectedTab = Pane.getSelectedIndex();

		switch (SelectedTab) {
			case 0: break;
			case 1: break;
			case 2:
				Log.updateLogTable(LogPath);
				gui.MediaRenamer.PilotUIManager.warn(2, false);
				break;
			default: break;
		}
		
	}

}
