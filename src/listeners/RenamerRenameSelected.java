package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.table.DefaultTableModel;

public class RenamerRenameSelected implements ActionListener, main.Settings {
	
	private static uimanagers.Log LogUIManager = new uimanagers.Log();
	private core.FileManager FileManager = new core.FileManager();
	private uimanagers.Pilot PilotUIManager = gui.MediaRenamer.PilotUIManager;
	private DefaultTableModel DataTableModel = gui.MediaRenamer.DataTableModel;
	private handlers.Media Media;
	
	private String SelectedMedia;
	private String SelectedLanguage;
	private String SelectedDatasource;
	private String OutputFormat = gui.MediaRenamer.OptionsOutputFormatField.getText();
	
	private String DirectoryPath;
	private File Directory;
	private Integer NumberOfScannedFiles;

	public void actionPerformed(ActionEvent arg0) {
		
		gui.MediaRenamer.RenamerScanDirectory.setEnabled(false);
		gui.MediaRenamer.RenamerCheckErrors.setEnabled(false);
		gui.MediaRenamer.RenamerRenameSelected.setEnabled(false);
		
		Directory = new File(gui.MediaRenamer.RenamerDirectoryField.getText());
		DirectoryPath = Directory.getAbsolutePath()+File.separator;
		
		SelectedMedia = gui.MediaRenamer.RenamerMediaList.getSelectedItem().toString();
		
		if (SelectedMedia != gui.MediaRenamer.OptionsMediaList.getSelectedItem().toString() || OutputFormat.equals("")) {
			SelectedLanguage = RulesManager.loadDefault(SelectedMedia, "language");
			SelectedDatasource = RulesManager.loadDefault(SelectedMedia, "datasource");
		} else {
			SelectedLanguage = gui.MediaRenamer.OptionsLanguagesList.getSelectedItem().toString();
			SelectedDatasource = gui.MediaRenamer.OptionsDatasourcesList.getSelectedItem().toString();
		}
		
		Media = new handlers.Media(SelectedMedia, SelectedLanguage, SelectedDatasource);
		
		NumberOfScannedFiles = 0;
		gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+DataTableModel.getRowCount());
		gui.MediaRenamer.RenamerScanProcess.setMaximum(DataTableModel.getRowCount());
		
		for (int row = 0; row < DataTableModel.getRowCount(); row++) {
			
			if ((Boolean) DataTableModel.getValueAt(row, 0)) {
				
				String OldPath = DirectoryPath+DataTableModel.getValueAt(row, 1)+"."+DataTableModel.getValueAt(row, 3);
				String NewName = (String) DataTableModel.getValueAt(row, 2);
				
				boolean Renamed = Media.renameFile(OldPath, NewName);
				
				if (!Renamed) {
					String OldName = FileManager.getFileNameWithoutExtension(new File(OldPath).getName());
					LogUIManager.updateLog(LogPath, "CannotRename", new String[]{OldName});
				}
			}
			
			NumberOfScannedFiles++;
			
			gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+DataTableModel.getRowCount());
			gui.MediaRenamer.RenamerScanProcess.setValue(NumberOfScannedFiles);
			
		}
		
		gui.MediaRenamer.RenamerScanDirectory.setEnabled(true);
		gui.MediaRenamer.RenamerCheckErrors.setEnabled(true);
		gui.MediaRenamer.RenamerRenameSelected.setEnabled(true);
		
		LogUIManager.updateLog(LogPath, "Done", new String[]{});
		PilotUIManager.warn(2, true);

	}

}
