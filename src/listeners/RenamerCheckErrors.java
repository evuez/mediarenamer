package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.table.DefaultTableModel;

public class RenamerCheckErrors implements ActionListener, main.Settings {
	
	private static uimanagers.Log LogUIManager = new uimanagers.Log();
	private uimanagers.Pilot PilotUIManager = gui.MediaRenamer.PilotUIManager;
	private DefaultTableModel DataTableModel = gui.MediaRenamer.DataTableModel;
		
	private Integer NumberOfScannedFiles;

	public void actionPerformed(ActionEvent arg0) {
			
			NumberOfScannedFiles = 0;
			gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+DataTableModel.getRowCount());
			gui.MediaRenamer.RenamerScanProcess.setMaximum(DataTableModel.getRowCount());
			
			gui.MediaRenamer.RenamerScanDirectory.setEnabled(false);
			gui.MediaRenamer.RenamerCheckErrors.setEnabled(false);
			gui.MediaRenamer.RenamerRenameSelected.setEnabled(false);
			
			for (int row = 0; row < DataTableModel.getRowCount(); row++) {
				
				String Name = (String) DataTableModel.getValueAt(row, 1);
				String NewName = (String) DataTableModel.getValueAt(row, 2);
				String Extension = (String) DataTableModel.getValueAt(row, 3);
				
				if (NewName.equals("") || Name.equals(NewName)) {
					DataTableModel.setValueAt(false, row, 0);
				}
				
				for (int checkrow = 0; checkrow < DataTableModel.getRowCount(); checkrow++) {
					if (row != checkrow &&
						(Boolean) DataTableModel.getValueAt(checkrow, 0) &&
						NewName.equals(DataTableModel.getValueAt(checkrow, 2).toString()) &&
						Extension.equals(DataTableModel.getValueAt(checkrow, 3).toString())) {
						
						DataTableModel.setValueAt(false, row, 0);
						
						LogUIManager.updateLog(LogPath, "Duplicate", new String[]{NewName});
						PilotUIManager.warn(2, true);
					}
				}
				
				NumberOfScannedFiles++;
				
				gui.MediaRenamer.RenamerScanProcess.setString(NumberOfScannedFiles.toString()+"/"+DataTableModel.getRowCount());
				gui.MediaRenamer.RenamerScanProcess.setValue(NumberOfScannedFiles);
				
			}
			
			gui.MediaRenamer.RenamerScanDirectory.setEnabled(true);
			gui.MediaRenamer.RenamerCheckErrors.setEnabled(true);
			gui.MediaRenamer.RenamerRenameSelected.setEnabled(true);

	}

}
