package main;
import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;



public class RulesManager implements main.Settings {
	
	private static String RulesPath;
	private static Document RulesDOM;
	
	public RulesManager(String PathToRules) {
		RulesPath = PathToRules;
		RulesDOM = initRules();
	}
	
	public static Document initRules() {
		
		try {
			File RulesForAPI = new File(RulesPath);
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document Rules = dBuilder.parse(RulesForAPI);
			Rules.getDocumentElement().normalize();
			return Rules;
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	public Object xpathObject(String XPath) {
		
		Object XPathObject = new Object();
		
		try {
			XPathFactory factory = XPathFactory.newInstance();
			XPath xpath = factory.newXPath();
			XPathExpression expr = xpath.compile(XPath);
			XPathObject = expr.evaluate(RulesDOM, XPathConstants.NODESET);
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return XPathObject;
		
	}
	/*
	public Object DOMObject() {
		
		Node Setup = RulesDOM.getElementsByTagName("setup");
		return null;
	}*/
	
	public void saveAsDefault(String Media, String DataType, String Value) {
		
		Object XPathObject = RulesManager.xpathObject(SetupXPath+"[@media='"+Media+"' and @type='"+DataType+"']");
		Node Default = (Node) ((NodeList) XPathObject).item(0);
		Default.setTextContent(Value);
		
		StreamResult DOMResult = new StreamResult(RulesPath);
		DOMSource DOMSource = new DOMSource(RulesDOM);
		
		Transformer Transformer;
		
		try {
			Transformer = TransformerFactory.newInstance().newTransformer();
			Transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			Transformer.transform(DOMSource, DOMResult);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public String loadDefault(String Media, String DataType) {
		
		Object XPathObject = RulesManager.xpathObject(SetupXPath+"[@media='"+Media+"' and @type='"+DataType+"']");
		Node Default = (Node) ((NodeList) XPathObject).item(0);
		
		return Default.getTextContent();
		
	}
}
