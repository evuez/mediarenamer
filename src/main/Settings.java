package main;


public interface Settings {

	public static final String RulesPath = "resources/rules.xml";
	public static final String LogPath = "resources/logs/";
	public static final RulesManager RulesManager = new RulesManager(RulesPath);
	public static final String SetupXPath = "//setup/data";
	
}
